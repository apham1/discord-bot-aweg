import discord
import asyncio
import random

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('-------------')

#Hangman variables (TEMP)
word_list = open('word_list.txt', 'r').read().strip().split(', ')

global is_playing_game, word_to_guess, split_word, word_length, guess_progress, letters_guessed, lives
is_playing_game = False

def add_points(message, points):
    with open('points.txt', 'r') as file:
        point_data = file.readlines()

    x = 0
    is_registered = False
    for line in point_data:
        data = line.split(':')
        if data[0] == message.author.id:
            data[1] = str(int(data[1]) + points) + '\n'
            point_data[x] = data[0] + ':' + data[1]
            is_registered = True
            break
        x += 1

    if is_registered == False:
        point_data.append(str(message.author.id) + ':' + str(points))

    with open('points.txt', 'w') as file:
        file.writelines(point_data)




def display_points(message):
    with open('points.txt', 'r') as file:
        point_data = file.readlines()

    for line in point_data:
        data = line.split(':')
        if data[0] == message.author.id:
            return data[1].strip('\n')

'''
def win_message(client, message):
    await client.send_message(message.channel, 'You guessed the word! (%s)' % word_to_guess)
    points = lives + word_length
    add_points(message, points)
'''

@client.event
async def on_message(message):
    global is_playing_game, word_to_guess, split_word, word_length, guess_progress, letters_guessed, lives

    if is_playing_game == True:
        if len(message.content) == 1 and message.content.isalpha():
            current_guess = str(message.content)
            correct_guess = False
            progress_str = ''

            for x in range(0, word_length):
                if current_guess == guess_progress[x][1]:
                    correct_guess = True
                    if guess_progress[x][0] == 0:
                        guess_progress[x][0] += 1
                        letters_guessed += 1

            if correct_guess == False:
                lives -= 1

            for x in range(0, word_length):
                if guess_progress[x][0] == 0:
                    progress_str += '- '
                elif guess_progress[x][0] == 1:
                    progress_str += guess_progress[x][1] + ' '
            await client.send_message(message.channel, progress_str + '    Lives Left: {0}'.format(lives))

            if letters_guessed == word_length:
                await client.send_message(message.channel, 'You guessed the word! ({0})'.format(word_to_guess))
                points = lives + word_length
                await client.send_message(message.channel, "You have {0} points! (+{1})".format(display_points(message)), points)
                add_points(message, points)
            if lives == 0:
                is_playing_game = False
                await client.send_message(message.channel, 'You ran out of lives! The word was %s.' % word_to_guess)
        elif str(message.content) == word_to_guess:
            await client.send_message(message.channel, 'You guessed the word! ({0})'.format(word_to_guess))
            points = lives + word_length
            await client.send_message(message.channel, "You have {0} points! (+{1})".format(display_points(message), points))
            add_points(message, points)
            is_playing_game = False



    #Prints Aweg
    if message.content.startswith('!aweg'):
        await client.send_message(message.channel, "Aweg")

    #Flips a coin printing heads or tails
    elif str(message.content) == '!flip':
        num = random.randint(0, 1)
        if num == 0:
            await client.send_message(message.channel, message.author.display_name + " got heads.")
        elif num == 1:
            await client.send_message(message.channel, message.author.display_name + " got tails.")
    
    #Randomly generates a number based on input
    elif message.content.startswith('!roll'):
        MAX_SHOWN_ROLLS = 10

        roll_input = message.content.split()
        if len(roll_input) > 1:
            roll_type = roll_input[1].lower()
            roll_type_list = list(roll_type)

            if 'd' in roll_type_list:
                split = roll_type.split('d')
                dice_amount = int(split[0])
                dice_sides = int(split[1])

                if dice_sides > 1000:
                    dice_sides = 1000
                if dice_amount > 100:
                    dice_amount = 100

                total_roll = 0
                roll_list = []
                for x in range (0, dice_amount):
                    current_roll = random.randint(1, dice_sides)
                    total_roll += current_roll
                    roll_list.append(str(current_roll))
                
                if len(roll_list) <= MAX_SHOWN_ROLLS:
                    roll_list_string = '[Rolls: ' + ', '.join(roll_list) + ']'
                else:
                    roll_list_string = '[Rolls: ' + ', '.join(roll_list[0:MAX_SHOWN_ROLLS - 1]) + ', ...]'
                await client.send_message(message.channel, message.author.display_name + " rolled {0}d{1} for {2}. {3}".format(dice_amount, dice_sides, total_roll, roll_list_string))

            elif '-' in roll_type_list:
                split = roll_type.split('-')
                min = int(split[0])
                max = int(split[1])

                if min > max:
                    min, max = max, min

                if min < 0:
                    min = 0
                if max > 1000000000:
                    max = 1000000000

                roll = random.randint(min, max)

                await client.send_message(message.channel, message.author.display_name + " rolled a {0}. ({1}-{2})".format(roll, min, max))

        else:
            roll = random.randint(1, 100)
            await client.send_message(message.channel, message.author.display_name + " rolled a {0}. (1-100)".format(roll))
    #Initialize hangman game
    elif message.content.startswith('!hangman') and is_playing_game == False:
        is_playing_game = True
        word_to_guess = word_list[random.randint(0, len(word_list) - 1)]
        split_word = list(word_to_guess)
        word_length = len(word_to_guess)
        guess_progress = []
        letters_guessed = 0
        lives = len(word_to_guess)

        if lives < 3:
            lives = 3

        for x in range(0, word_length):
            guess_progress.append([0, split_word[x]])

        await client.send_message(message.channel, 'Hangman')

        progress_str = ''
        for x in range(0, word_length):
            progress_str = progress_str + '- '

        await client.send_message(message.channel, "{0}    Lives Left: {1}".format(progress_str, lives))
    elif message.content.startswith('!points'):
        await client.send_message(message.channel, "{0} has {1} points." % (message.author.display_name, display_points(message)))
    elif message.content.startswith('!hint'):
        await client.send_message(message.channel, "The word is: {0}".format(word_to_guess))
                


client.run(open('token.txt', 'r').read())